import { Movie } from './movie';

export const Movies: Movie[] = [
  { title: 'Titanic', year: 1997 },
  { title: 'Matrix', year: 1999 },
  { title: 'Avengers: Endgame', year: 2019 },
  { title: 'Cinderella', year: 2015 },
  { title: 'Fast Five', year: 2011 },
  { title: 'Rebecca', year: 1940 },
  { title: 'Les Miserables', year: 2012 },
  { title: 'i, Robot', year: 2004 },
  { title: 'Sabrina', year: 1954 },
  { title: 'Mallrats', year: 1995 }
];